import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import InputSearch from './components/home/InputSearch'
import JobDetails from './components/home/JobDetails'
import { store } from './store/store'

Vue.config.productionTip = false

export const bus = new Vue();

Vue.use(VueRouter);

const routes = [
  {
    path: '/details', component: JobDetails
  },
  {
    path: '/', component: InputSearch
  }
];

const router = new VueRouter({
  routes
});

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
