import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

// use to store the state of our app (selected jobpost and result of the search)
export const store = new Vuex.Store({
    state: {
        selectedJobPost: {},
        searchResult: {}
    },
    mutations: {
        selected(state, selectedItem) {
            this.state.selectedJobPost = selectedItem
        },
        getResults(state, results) {
            this.state.searchResult = results;
        }
    },
    getters: {
        selectedJobPost: state => state.selectedJobPost,
        results: state => state.searchResult
    }
});